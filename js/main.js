// main.js

// Initialize the whole scene with a configuration
function init(config) {
    var scene, camera, tiles, points, labels;

    var min_height = 100000; // to move the heights down by a uniform value.
    var spread = 0;
    var NO_DATA = -9999.0
    // tile constants
    var tile_size = 256;

    
    var z = config.zoom;
    var xmin = config.left - spread;
    var xmax = config.right + spread;
    var ymin = config.bottom - spread;
    var ymax = config.top + spread;
    
    // num tiles, width and height <--> x and y
    var x_tiles = xmax - xmin + 1;
    var y_tiles = ymax - ymin + 1;
    
    // world sizes
    var w_width = tile_size * x_tiles;
    var w_height = tile_size * y_tiles;
    
    // scene graph, camera and builtin WebGL renderer
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 75, document.body.clientWidth/document.body.clientHeight, 0.1, 2500 );
    
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize( document.body.clientWidth, document.body.clientHeight);
    document.body.appendChild(renderer.domElement);
    
    var controls = new THREE.OrbitControls(camera, renderer.domElement)
    controls.maxDistance = 800
    controls.minDistance = 50
    controls.maxPolarAngle = Math.PI / 2.1
    
    var loader = new THREE.TextureLoader()

    // groups for housing 3d objects
    tiles = new THREE.Group();
    points = new THREE.Group();
    labels = new THREE.Group();
    
    function computeHeights(texture) {
    
        // Access Image object from THREE.Texture
        var image = texture.image
        var w = image.naturalWidth
        var h = image.naturalHeight
    
        // Instantiate a canvas and extract the data from within
        var canvas = document.createElement('canvas')
        canvas.width = w
        canvas.height = h
        var ctx = canvas.getContext('2d')
        ctx.drawImage(image, 0, 0, w, h)
        var data = ctx.getImageData(0, 0, w, h).data
    
        // Allocate space for the height information
        var heights = new Float32Array(w * h)
        var idx;
        for (var y = 0; y < h; ++y) {
            for (var x = 0; x < w; ++x) {
                idx = (x + y * w) * 4;        
                heights[x + y * w] = ((data[idx] * 256) +  data[idx+1] + (data[idx+2] / 256) - 32768) / 256.0;
            }
        }
    
        // Free the resources and return
        data = ctx = canvas = null
        return heights
    }
    
    function createOneTile(x_idx,y_idx, x_offset, y_offset) {
        var height_tex_url = 'https://s3.amazonaws.com/elevation-tiles-prod/terrarium/' + z + '/' + x_idx + '/' + y_idx + '.png'
        var data_tex_url = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/' + z + '/' + y_idx + '/' + x_idx
        
        var coords = new L.Point(x,y);
        coords.z = config.zoom;
        var resilience_tex_url = config.layer.getTileUrl(coords);

        loader.load(height_tex_url, function (h_texture) {
            loader.load(data_tex_url, function (d_texture) {
            loader.load(resilience_tex_url, function (r_texture) {    
                
                // Tile geometry
                var geometry = new THREE.PlaneBufferGeometry(tile_size, tile_size, tile_size - 1, tile_size - 1);

                // Compute heights and update global minimum
                var heights = computeHeights(h_texture);
                var h_min = Math.min(...heights);
                min_height = (h_min < min_height) ? h_min : min_height;

                // Set heights for this tile
                var vertices = geometry.getAttribute('position')
                for (var i = 0; i < vertices.count; i++) {
                    vertices.setZ(i, heights[i]);
                }
                vertices.needsUpdate = true;  
                geometry.rotateX(-Math.PI / 2)

                var material = new THREE.ShaderMaterial(
                    {
                        uniforms: {
                            't_data': {value: d_texture},
                            'r_data': {value: r_texture},
                            'show_resilience': {value: false},
                            'elev_factor': {value: 1.0}
                        },
                        vertexShader: document.getElementById('vertexShader').textContent,
                        fragmentShader: document.getElementById('fragmentShader').textContent
                    });

                var tile = new THREE.Mesh(geometry, material);
                tile.translateOnAxis(new THREE.Vector3(1, 0, 0), x_offset);
                tile.translateOnAxis(new THREE.Vector3(0, 0, 1), y_offset);
                tile.userData = {'x': x_idx, 'y': y_idx};
                tiles.add(tile);

                // Check if setup is complete
                num_requests--;
                if (num_requests == 0) {
                    scene.add(tiles);
                    tiles.translateOnAxis(new THREE.Vector3(0,1,0), -min_height);
                    console.log('Loaded in ' + String((new Date().getTime() - start) / 1000) + ' seconds');
                    $('#loading_div').hide();
                    alertify.logPosition("top right")
                    .delay(0)
                    .closeLogOnClick(true)
                    .log("Click on each station to see categorical data.");
                    createLights();
                    createTilePoints(true);
                    initChecks();
                }
            })
            })
        })
    }
    
    var local_x_offset = 0;
    var local_y_offset = 0;
    
    var start = new Date().getTime();
    var num_requests = 0;
    
    // add tiles in succession
    for (var x = xmin; x <= xmax; x++) {
        local_y_offset = 0;
        for (var y = ymin; y <= ymax; y++) {
        num_requests++;
        createOneTile(x, y, local_x_offset, local_y_offset);
            local_y_offset += tile_size;
        }
        local_x_offset += tile_size;
    }
    
    // move the whole world to center the map
    scene.translateOnAxis(new THREE.Vector3(1,0,0), - w_width / 2 + tile_size/2);
    scene.translateOnAxis(new THREE.Vector3(0,0,1), - w_height / 2 + tile_size/2);
    
    // move the camera
    camera.position.y = 400;

    function createLights() {
        light = new THREE.HemisphereLight();
        scene.add(light);
    }

    // Create stations
    function createTilePoints(update) {

        // Same geometry for each mesh
        var point_geo = new THREE.SphereBufferGeometry(.8, 8, 8);

        function createOnePoint(tile, data) {
            var point_material = new THREE.MeshLambertMaterial({color: 0x156289});

            var sx = tile.position.x + tile_size * data.pct_x - tile_size / 2;
            var sz = tile.position.z + tile_size * data.pct_y - tile_size / 2;
            var h = getHeightFromTile(tile, data);
            var sy = h - min_height + .1;

            var mesh = new THREE.Mesh(point_geo, point_material);
            mesh.userData = data;
            mesh.userData.height = h * 256.0;
            mesh.position.x = sx;
            mesh.position.y = sy;
            mesh.position.z = sz;

            points.add(mesh);

        }

        for (var idx = 0; idx < tiles.children.length; idx++) {
            var tile = tiles.children[idx];
            var mesh_data = tile.userData;
            for (var i = 0; i < config.tile_data.length; i++) {
                var tile_data = config.tile_data[i];
                if (tile_data.x == mesh_data.x && tile_data.y == mesh_data.y) {
                    createOnePoint(tile, tile_data);
                }
            }
        }
        scene.add(points);

        if (update) {
            updateTilePoints();
        }
    }

    var grey = new THREE.Color('grey');

    function updateTilePoints() {
        // TODO - use a callback
        
        for (var i = 0; i < points.children.length; i++) {

            var point_mat = points.children[i].material;
            var point_data = points.children[i].userData;
            var mesh = points.children[i];
            var id = point_data.id;
            var consensus = point_data.site_data.consensus;
            var confidence = Number(point_data.site_data.confidence);
            var gradient_color;
            var left;
            var white = {r:255,g:255,b:255};

            function findColorBetween(left, right, percentage) {
                newColor = {};
                components = ["r", "g", "b"];
                for (var i = 0; i < components.length; i++) {
                    c = components[i];
                    newColor[c] = Math.round(left[c] + (right[c] - left[c]) * percentage / 100);
                }
                return newColor;
            }

            function rgbAsCss(obj) {
                return "rgb("+obj.r+", "+obj.g+", "+obj.b+")";
            }

            if (consensus == 'increase') {
                var nada = 'nada';
                gradient_color = new THREE.Color(rgbAsCss(findColorBetween(white,{'r':0,'g':0,'b':255},confidence / 20 * 100)));
            }
            else if (consensus == 'decrease') {
                gradient_color = new THREE.Color(rgbAsCss(findColorBetween(white,{'r':255,'g':0,'b':0},confidence / 20 * 100)));
                confidence = confidence * -1;
            } else {
                confidence = 0;
                gradient_color = grey;
            }

            point_mat.color = gradient_color;

            // Add labels?
            var message = 'Site ' +  point_data.site_data.site;
            var label =  makeTextSprite(message, mesh.position.x, mesh.position.y + 1, mesh.position.z,
                { // parameters
                    fontsize: 18,
                    fontface: "Georgia",
                    borderThickness: 4,
                    textColor: {r: 255, g: 255, b: 255, a: 1.0},
                    fillColor: {r: point_mat.color.r * 255, g: point_mat.color.g * 255, b: point_mat.color.b * 255, a: 1.0},
                    radius: 0,
                    vAlign: "bottom",
                    hAlign: "center"
                }
            );
            labels.add(label);
        }

        scene.add(labels);

    }

    function initChecks() {
    
        // checkboxes
        $('#low').on('change', showOrHidePoints);
        $('#moderate').on('change', showOrHidePoints);
        $('#high').on('change', showOrHidePoints);
    
        // numbers
        $('#highmodt').on('change', showOrHidePoints);
        $('#modlowt').on('change', showOrHidePoints);

        $('#hide').on('change', function() {
            labels.visible = !$(this).prop('checked');
        })

        $('#resilience').on('change', function() {
            var val = $(this).prop('checked');

            if (val) {
                $('#res_legend').show();
            } else {
                $('#res_legend').hide();
            }

            for (var i = 0; i < tiles.children.length; i++) {
                tiles.children[i].material.uniforms.show_resilience.value = val;
                tiles.children[i].material.needsUpdate = true;
            }
        })
    }

    function showOrHidePoints() {

        var lowMedium = $('#modlowt').val();
        var mediumHigh = $('#highmodt').val();

        var showLow = $('#low').prop('checked');
        var showMod = $('#moderate').prop('checked');
        var showHigh = $('#high').prop('checked');

        if (!points.children.length) return;

        for (var i = 0; i < points.children.length; i++) {
            var point = points.children[i];
            var confidence = Number(point.userData.site_data.confidence);
            var visible = true;
            
            if (confidence < lowMedium && !showLow) {
                visible = false;
            }
            else if (confidence < mediumHigh && !showMod) {
                visible = false;
            }
            else if (confidence >= mediumHigh && !showHigh) {   // confidence >= mediumHigh always true here
                visible = false;
            }

            point.visible = visible;
            labels.children[i].visible = visible;
        }

    }

    function onDocumentMouseDown( event ) {    
        var mouse3D = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1,   
                                    -( event.clientY / window.innerHeight ) * 2 + 1,  
                                    0.5 );     
        var raycaster =  new THREE.Raycaster();                                        
        raycaster.setFromCamera( mouse3D, camera );
        var point_intersects = raycaster.intersectObjects( points.children );
        var tile_intersects = raycaster.intersectObjects( tiles.children );

        if ( point_intersects.length > 0 ) {
            // TODO - add a callback for when a point is intersected
            //intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
            var data = point_intersects[0].object.userData;
            var t_idx = $('#time_slider').slider('option','value');
            alertify.logPosition("top right")
                .delay(0)
                .maxLogItems(4)
                .closeLogOnClick(true)
                .log('Station ' + data.site_data.site)
                .log('Elevation (m) = ' + String(data.height.toFixed(1)))
                .log('Consensus = ' + data.site_data.consensus)
                .log('Confidence = ' + String(data.site_data.confidence));
        }

        if (tile_intersects.length > 0) {
            // TODO - add callback for when a tile is intersected

        }
    }

    document.addEventListener('mousedown', onDocumentMouseDown, false);
    
    // Used to update the tile layer's image texture.
    function updateOneTile(tile) {
        var tile_data = tile.userData;
    }
    
    function getHeightFromTile(tile, val) {
        var idx = Math.round(tile_size * val.pct_x) + tile_size * Math.round(tile_size * val.pct_y);
        return tile.geometry.getAttribute('position').getY(idx);
    }

    var render = function () {
        requestAnimationFrame(render)
        controls.update();
        renderer.render(scene, camera);
    };
    
    function resize() {
        renderer.setSize(document.body.clientWidth, document.body.clientHeight);
        camera.aspect = document.body.clientWidth / document.body.clientHeight;
        camera.updateProjectionMatrix();
        render();
    }
    
    window.addEventListener('resize', resize, false);
    render();
}

// Retrieve location data and initialize the webgl scene
$.getJSON('/js/confidence_categories.json').done(function(res) {

    // Always init map first
    var map = L.map('map').setView([0, 0], 5);  // Doesn't matter, it's hiding
    var layer = L.tileLayer.wms('https://www.sciencebase.gov/arcgis/services/Catalog/55229c34e4b027f0aee3cfa5/MapServer/WMSServer?', {
        layers: '0',

        // Set these to get the proper transparency we need.
        transparent: true,
        format: 'image/png' 
    }).addTo(map);

    var config = {};
    config.layer = layer;
    config._orig_res = res;
    var zoom = 5;
    config.zoom = zoom;
    config.tile_data = [];

    var lats = [];
    var lons = [];

    for (var i = 0; i < res.length; i++) {

        var lat = Number(res[i].Lat);
        var lon = Number(res[i].Lon);

        lats.push(lat);
        lons.push(lon);

        // Get point's tile
        var x = lon2tile(lon, zoom);
        var y = lat2tile(lat, zoom);
        var coords = new L.Point(x,y);
        coords.z = zoom;
        var url = layer.getTileUrl(coords);

        // Get tile bbox
        var n = tile2lat(y, zoom);
        var s = tile2lat(y+1, zoom);
        var w = tile2lon(x, zoom);
        var e = tile2lon(x+1, zoom);

        // Get position in tile
        var pct_x = (lon - w) / (e - w);
        var pct_y = 1 - ((lat - s) / (n - s));

        config.tile_data.push({
            'id': i,
            'x': x,
            'y': y,
            'lat': lat,
            'lon': lon,
            'pct_x': pct_x,
            'pct_y': pct_y,
            'url': url,
            'site_data': {
                'consensus': res[i].consensus,
                'confidence': res[i].confidence,
                'site': res[i].site
            }
        })
    }

    /* TODO - move this to be part of the init function */
    // Determine world tiles
    var north_edge = Math.max(...lats);
    var south_edge = Math.min(...lats);
    var west_edge = Math.min(...lons);
    var east_edge = Math.max(...lons);

    var min = map.project(new L.LatLng(north_edge, west_edge), zoom).divideBy(256).floor();
    var max = map.project(new L.LatLng(south_edge, east_edge), zoom).divideBy(256).floor();

    var top_tile    = lat2tile(north_edge, zoom); // eg.lat2tile(34.422, 9);
    var left_tile   = lon2tile(west_edge, zoom);
    var bottom_tile = lat2tile(south_edge, zoom);
    var right_tile  = lon2tile(east_edge, zoom);

    config.zoom = zoom;
    config.top = max.y;
    config.left = min.x;
    config.bottom = min.y
    config.right = max.x;

    init(config);
});
